import React from 'react';
import launch from './launch.png';
import './Card.css';

const Card =({name , age, style, id }) =>{


return(
  <div className ='tc bg-light-green dib br3 ph5 pv4 ma2 grow bw2 shadow-5'>



  
    <img  src={launch} className="launch" />
    <div>
      <h3 className='blocktitle'> {  `${name} : click on the shuttle to launch!`} </h3>
      <p> { `Age : ${age}`} </p>
       <p> {`Style : ${style}`} </p>
      </div>
      </div>)


}

export default Card
